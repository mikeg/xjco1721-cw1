package comp1721.cwk1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class CovidChart extends Application {
    private static String fileName;
    @Override
    public void start(Stage stage) throws IOException {
        // temp output file (active cases) name
        String tempFile = "tempActiveCases.csv";
        // create a CovidDataset object and load data
        CovidDataset loadedDataset = new CovidDataset();
        loadedDataset.readDailyCases(fileName);
        // output active cases to a temp file
        loadedDataset.writeActiveCases(tempFile);

        // init 2 ArrayList to store active cases data from csv
        ArrayList <Double> daysOfYearList = new ArrayList <Double>();
        ArrayList <Double> totalRecordList = new ArrayList <Double>();

        // read temp csv (active cases)
        try (BufferedReader br = new BufferedReader(new FileReader(tempFile))) {
            br.readLine();  // remove header
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                int daysOfYear = LocalDate.parse(values[0]).getDayOfYear();
                daysOfYearList.add((double) daysOfYear);
                int totalRecord = Integer.parseInt(values[1]) + Integer.parseInt(values[2]) +
                        Integer.parseInt(values[3]);
                totalRecordList.add((double) totalRecord);
            }
        }

        // remove temp csv file
        File removeTemp = new File(tempFile);
        removeTemp.delete();

        // Define x axis
        NumberAxis xAxis = new NumberAxis(daysOfYearList.get(0) - daysOfYearList.get(0) % 5,
                daysOfYearList.get(daysOfYearList.size() - 1), 5);
        xAxis.setLabel("Days of year");

        // find max and min
        double maxCases = 0;
        for (int i = 0; i < totalRecordList.size(); i ++) {
            if (totalRecordList.get(i) > maxCases) maxCases = totalRecordList.get(i);
        }

        // Define y axis
        NumberAxis yAxis = new NumberAxis(0, maxCases, 50);
        yAxis.setLabel("Active Cases");

        // Init a LineChart object with above data
        LineChart linechart = new LineChart(xAxis, yAxis);

        // Define a new series
        XYChart.Series series = new XYChart.Series();
        series.setName(fileName);

        // add data
        for (int i = 0; i < daysOfYearList.size(); i ++) {
            series.getData().add(new XYChart.Data(daysOfYearList.get(i), totalRecordList.get(i)));
        }

        // add data to series
        linechart.getData().add(series);

        // create scene
        Scene scene = new Scene(linechart, 600, 400);

        // set title
        stage.setTitle("Active Coronavirus Cases, University of Leeds");

        // pop up the window
        stage.setScene(scene);
        stage.show();
    }
    public static void main(String args[]){
        fileName = args[0];
        launch(args);
    }
}