package comp1721.cwk1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class CovidDataset {
    private ArrayList<CaseRecord> dataSetArray;
    public CovidDataset(){
        dataSetArray = new ArrayList();
    }
    public int size(){
        return dataSetArray.size();
    }

    public CaseRecord getRecord (int index) {
        if(index >= dataSetArray.size() || index < 0){
            // out of arraylist length
            throw new DatasetException("Supplied index is not valid");
        }
        else {
            return dataSetArray.get(index);
        }
    }

    public void addRecord(CaseRecord rec) {
        dataSetArray.add(rec);
    }

    public CaseRecord dailyCasesOn(LocalDate day) {
        for (Object o : dataSetArray) {
            CaseRecord tempRecord = (CaseRecord) o;
            if (tempRecord.getDate() == day) {
                return tempRecord;
            }
        }
        throw new DatasetException("Data with given date cannot be found");
    }

    public void readDailyCases(String filename) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            // remove header
            br.readLine();
            // empty original array
            dataSetArray.clear();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if (values.length != 4) {
                    throw new DatasetException("A column is missing in CSV");
                }
                LocalDate recordDate = LocalDate.parse(values[0]);
                int recordStaff = Integer.parseInt(values[1]);
                int recordStu = Integer.parseInt(values[2]);
                int recordOther = Integer.parseInt(values[3]);
                CaseRecord tempRecord = new CaseRecord(recordDate, recordStaff,
                        recordStu, recordOther);
                addRecord(tempRecord);
            }
        }
    }
    public void writeActiveCases(String filename) throws IOException {
        if (dataSetArray.size() < 10) {
            throw new DatasetException("At least 10 records are needed to write to csv");
        }
        File csvWrite = new File(filename);
        try (PrintWriter pw = new PrintWriter(csvWrite)) {
            // print header
            pw.println("Date,Staff,Students,Other");
            for (int i = 9; i < dataSetArray.size(); i++) {
                CaseRecord todayRecord = dataSetArray.get(i);
                DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String dateStr = todayRecord.getDate().format(fmt);
                int staffCasesAccumulate = 0;
                int studentCasesAccumulate = 0;
                int otherCasesAccumulate = 0;
                for (int j = 0; j < 10; j ++){
                    CaseRecord tempRecord = dataSetArray.get(i - (9 - j));
                    staffCasesAccumulate += tempRecord.getStaffCases();
                    studentCasesAccumulate += tempRecord.getStudentCases();
                    otherCasesAccumulate += tempRecord.getOtherCases();
                }
                String writeLine = dateStr + "," +
                        staffCasesAccumulate + "," +
                        studentCasesAccumulate + "," +
                        otherCasesAccumulate;
                pw.println(writeLine);
            }
        }

    }
}
