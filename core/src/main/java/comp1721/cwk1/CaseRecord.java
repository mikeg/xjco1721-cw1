package comp1721.cwk1;

import java.time.LocalDate;

public class CaseRecord {
    private LocalDate caseDate;
    private int staffCases;
    private int studentCases;
    private int otherCases;
    public CaseRecord(LocalDate date, int staff, int student, int others) {
        if (staff < 0 || student < 0 || others < 0) {
            throw new DatasetException("Number of cases cannot be negative");
        }
        caseDate=date;
        staffCases=staff;
        studentCases=student;
        otherCases=others;
    }
    public LocalDate getDate() {
        return caseDate;
    }

    public int getStaffCases () {
        return staffCases;
    }

    public int getStudentCases() {
        return studentCases;
    }

    public int getOtherCases() {
        return otherCases;
    }

    public int totalCases () {
        return staffCases + studentCases + otherCases;
    }
    @Override
    public String toString() {
        return caseDate + ": " +
                staffCases + " staff, " +
                studentCases + " students, " +
                otherCases + " other";
    }

}
