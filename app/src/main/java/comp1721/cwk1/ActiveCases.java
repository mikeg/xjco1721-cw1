package comp1721.cwk1;

import java.io.IOException;

public class ActiveCases {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Sorry, wrong number of parameters are provided");
            System.exit(-1);
        }
        CovidDataset cDataSet = new CovidDataset();
        try {
            cDataSet.readDailyCases(args[0]);
            cDataSet.writeActiveCases(args[1]);
            System.out.println(cDataSet.size());
        } catch (IOException e) {
            System.out.println("Input file cannot be found");
            System.exit(-1);
        } catch (DatasetException e) {
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }
}
